FROM python:3.10-slim as build
ENV POETRY_VERSION=1.1.13

RUN pip install poetry==$POETRY_VERSION
COPY ./pyproject.toml ./poetry.lock ./
RUN poetry install
RUN poetry add uvloop

COPY . .
ENTRYPOINT ["poetry", "run", "uvicorn", "main:app", "--loop", "uvloop", "--host", "0.0.0.0", "--port", "8000"]
