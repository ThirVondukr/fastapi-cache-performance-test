#FastAPI Cache Performance Tests
All endpoints return 200 pydantic models, ~80KB in size

### Using response_model parameter
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/simple/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/simple/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.02s   586.34ms   1.99s    56.96%
    Req/Sec    16.62     13.75    50.00     65.86%
  1223 requests in 30.04s, 103.26MB read
  Socket errors: connect 0, read 0, write 0, timeout 1144
Requests/sec:     40.71
Transfer/sec:      3.44MB
```
### Using responses parameter
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/no-response-model/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/no-response-model/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.44s   203.48ms   1.99s    94.92%
    Req/Sec    14.37     13.08    70.00     76.92%
  1926 requests in 30.04s, 162.61MB read
  Socket errors: connect 0, read 0, write 0, timeout 18
Requests/sec:     64.11
Transfer/sec:      5.41MB
```
### Using ORJSONResponse
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/no-response-model-orjson/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/no-response-model-orjson/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.35s   146.52ms   1.50s    96.30%
    Req/Sec    21.54     19.01    80.00     70.78%
  2071 requests in 30.04s, 174.88MB read
  Socket errors: connect 0, read 0, write 0, timeout 19
Requests/sec:     68.93
Transfer/sec:      5.82MB
```
### FastAPI Cache with response_model
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/fastapi-cache-with-response-model/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/fastapi-cache-with-response-model/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.49s   420.51ms   2.00s    66.26%
    Req/Sec     7.55      5.72    40.00     85.46%
  1230 requests in 30.05s, 103.85MB read
  Socket errors: connect 0, read 0, write 0, timeout 821
Requests/sec:     40.94
Transfer/sec:      3.46MB
```

### FastAPI Cache without response_model
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/fastapi-cache/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/fastapi-cache/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.08s   223.62ms   1.99s    70.08%
    Req/Sec    13.16      7.81    50.00     72.87%
  2541 requests in 30.04s, 214.53MB read
  Socket errors: connect 0, read 0, write 0, timeout 64
Requests/sec:     84.60
Transfer/sec:      7.14MB
```
### FastAPI Cache with custom Coder class
```
doctor@main:~/wrk$ ./wrk http://127.0.0.1:8000/fastapi-cache-custom-coder/ -t 8 -c100 -d30s
Running 30s test @ http://127.0.0.1:8000/fastapi-cache-custom-coder/
  8 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   224.31ms   34.75ms 493.49ms   73.92%
    Req/Sec    53.90     24.18   111.00     66.11%
  12805 requests in 30.04s, 1.20GB read
Requests/sec:    426.32
Transfer/sec:     40.88MB
```
