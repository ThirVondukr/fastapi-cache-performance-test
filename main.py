import json
import random
import string
from typing import Any

import aioredis
import uvicorn
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.responses import ORJSONResponse
from fastapi_cache import FastAPICache, Coder
from fastapi_cache.backends.redis import RedisBackend
from fastapi_cache.decorator import cache
from pydantic import BaseModel
from starlette.responses import Response

app = FastAPI()


class SomeModel(BaseModel):
    field0: str
    field1: str
    field2: str
    field3: str
    field4: str
    field5: str
    field6: str
    field7: str
    field8: str
    field9: str


def generate_test_data():
    return {
        field: "".join(random.choices(string.ascii_letters, k=32))
        for field in SomeModel.__fields__
    }


test_data = [
    SomeModel(**generate_test_data()) for _ in range(200)
]


class ByteJsonResponse(Response):
    media_type = "application/json"


class ByteCoder(Coder):
    @classmethod
    def encode(cls, value: Any):
        return json.dumps(jsonable_encoder(value))

    @classmethod
    def decode(cls, value: Any):
        return value


@app.on_event("startup")
async def startup():
    redis = aioredis.from_url("redis://redis", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")


@app.get("/simple/", response_model=list[SomeModel])
async def simple():
    return test_data


@app.get("/no-response-model/", responses={200: {"model": list[SomeModel]}})
async def no_response_model():
    return test_data


@app.get(
    "/no-response-model-orjson/",
    responses={200: {"model": list[SomeModel]}},
    response_class=ORJSONResponse,
)
async def no_response_model():
    return test_data


@app.get(
    "/fastapi-cache-with-response-model/",
    response_model=list[SomeModel],
)
@cache(expire=360)
async def fastapi_cache_():
    return test_data


@app.get(
    "/fastapi-cache/",
    responses={200: {"model": list[SomeModel]}},
)
@cache(expire=360)
async def fastapi_cache_():
    return test_data


@app.get(
    "/fastapi-cache-custom-coder/",
    responses={200: {"model": list[SomeModel]}},
)
@cache(expire=360, coder=ByteCoder)
async def fastapi_cache_custom_coder():
    return test_data


if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=9000)
